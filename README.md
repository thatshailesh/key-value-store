# Title

Simple version controlled key-value store module using Node.js, Express.js and Redis
## Getting Started

- Clone the repo
- cd && npm install

### Prerequisites

```
Redis - v4.0.11
node - v10.6.0
```

### Installing

#### Redis Installation Mac OSX
```
brew install redis
```

#### Other dependencies
```
npm install mocha eslint -g
```

## Run

```
DEBUG=key-value:* npm start
```
## Running the tests

```
npm test
```
## Deployment

Deployed here on [Heroku](https://key-value.herokuapp.com/)


## Authors

* **Shailesh Shekhawat**  - [Github](https://github.com/thatshailesh)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details