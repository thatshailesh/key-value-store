
const express = require('express');
const expressValidator = require('express-validator')
const cookieParser = require('cookie-parser');
const logger = require('morgan');
// const mongoose = require('mongoose')

const app = express();
require('dotenv').config()


app.use(logger('dev'));
app.use(express.json());
app.use(expressValidator())
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());


app.route('/')
  .all((req, res) => {
    res.send('Welcome to Key Value Store')
  })

app.use('/', require('./src/router'))

// catch 404 and forward to error handler
// app.use((req, res, next) => {
//   next(createError(404));
// });

// error handler
app.use((error, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = error.message;
  res.locals.error = req.app.get('env') === 'development' ? error : {};

  // render the error page
  res.status(error.status || 500);
  res.json({ error });
});

process.on('unhandledRejection', error => {
  console.log('Something bad happened', error);
});

module.exports = app;
