
const { query, param } = require('express-validator/check')
const { sanitizeParam } = require('express-validator/filter')
const Redis = require('../libs/redis')
const { bodyValidationHandler } = require('../utils/validator')

const { REDIS_URL } = process.env

const redisClient = new Redis({ url: REDIS_URL })

const hashName = process.env.REDIS_HASH_NAME
const setName = process.env.REDIS_SET_NAME

exports.create = (req, res, next) => {

    const timestamp = new Date(new Date().toUTCString()).getTime()

    const randomKey = 'x:id'

    return redisClient.getId(randomKey)
        .then(id => {
            const hashWithId = `${hashName}-${id}`

            return redisClient.setRedis(hashWithId, JSON.stringify(req.body))
                .then(() => redisClient.setAdd(setName, timestamp, hashWithId))
        })
        .then(() => {
            const key = Object.keys(req.body)[0]
            const value = req.body[key]
            res.json({
                key,
                value,
                timestamp,
            })
        })
        .catch(next)
}

exports.read = (req, res, next) => {
    req
        .getValidationResult()
        .then(bodyValidationHandler())
        .then(() => {
            if (req.query.timestamp)
                return redisClient.getMembersOfSet(setName, 0, req.query.timestamp)

            return redisClient.getRecentMemberOfSet(setName)

        })
        .then(([result]) => redisClient.getRedis(result))
        .then(resp => {
            const { myKey } = req.params

            if (resp) {
                let obj = null

                try {
                    obj = JSON.parse(resp)
                } catch (e) {
                    obj = resp
                }

                return res.json({ value: obj[myKey] ? obj[myKey] : "" })
            }
            return res.json({ value: '' })
        })
        .catch(next)

}


exports.validate = method => {
    switch (method) {
        case 'read': {
            return [
                sanitizeParam('myKey').trim(),
                param('myKey').exists()
                    .custom(() => redisClient.getRecentMemberOfSet(setName)
                        .then(result => {

                            if (!result.length) throw new Error('Not Found')

                            return true
                        })),
                query('timestamp', 'Date cannot be in future')
                    .optional()
                    .custom(val => {
                        const year = new Date(val).getUTCFullYear()
                        if ((new Date(val) > new Date()) || new Date(val) && year < 2000) throw new Error('Invalid time')

                        return redisClient.getMembersOfSet(setName, 0, val)
                            .then(result => {
                                if (!result.length) throw new Error('Invalid time')

                                return true
                            })
                    }),
            ]
        }
        default: {
            return []
        }
    }
}

