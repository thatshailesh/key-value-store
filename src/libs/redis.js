const redis = require("redis")
const bluebird = require('bluebird')

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);


class Redis {
    constructor(opt = {}) {
        const { url = null } = opt

        if (url)
            this.client = redis.createClient(url)
        else
            this.client = redis.createClient()


        this.client.on("error", err => {
            console.log(`Error ${err}`)
            throw new Error(err)
        })

        return this
    }

    getRedis(key) {
        return this.client.getAsync(key)
    }

    setRedis(key, value) {
        return this.client.setAsync(key, value)
    }

    hashSet(hash, value) {
        return this.client.hmsetAsync(hash, value)
    }

    hashGetAll(hash) {
        return this.client.hgetallAsync(hash)
    }

    hashGet(hash, key) {
        return this.client.hgetAsync(hash, key)
    }

    setAdd(setName, score, value) {
        return this.client.zaddAsync(setName, score, value)
    }

    getMembersOfSet(setName, min, max) {
        return this.client.zrangeAsync(setName, min, max)
    }

    getId(key) {
        return this.client.incrAsync(key)
    }

    getRecentMemberOfSet(setName) {
        return this.client.zrevrangebyscoreAsync(setName, '+inf', '-inf', 'LIMIT', '0', '1')
    }
}

// const r1 = new Redis()
// const obj = {
//     x: {
//         y: {
//             z: {
//                 a: {
//                     g: 2,
//                 },
//             },
//         },
//     },
// }
// r1.setRedis('temp', JSON.stringify(obj))
//     .then(res => {
//         r1.getRedis('temp')
//             .then(resul => {
//                 console.log('Wooo ->> ', JSON.parse(resul))
//             })
//     })
//     .catch(err => {
//         console.log('Err ->> ', err)
//     })
//     })

module.exports = Redis