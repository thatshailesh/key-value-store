const router = require('express').Router()
const { NotFoundError } = require('./errors')
const Routes = require('./routes')

router.use('/version', (req, res) => {
    const { version } = require('../package.json') // eslint-disable-line
    const { version: n } = process
    res.jsonOK({ version, n })
})

router.use('/object', Routes.store)

// Catch all other undefined routes
router.all('*', (req, res, next) => {
    next(new NotFoundError('API not found.'))
})

module.exports = router