const router = require('express').Router()

const storeController = require('../controllers/store')

/**
 * @api {post} /object Create Object in Store
 * @apiName Create new Object
 * @apiGroup Objects
 *
 * @apiParam  {String} key Store key
 * @apiParam  {String} [value] value of specified key store
 *
 * @apiSuccess (200) {Object} mixed `Store` object
 *
 */
router.post(
    '/',
    storeController.create
)

/**
 * @api {get} /object/:myKey Create Object in Store
 * @apiName Create new Object
 * @apiGroup Objects
 *
 * @apiParam  {String} key Store key
 * @apiParam  {timestamp} [timestamp] Timestamp in UTC
 *
 * @apiSuccess (200) {Object} mixed `Store` object value
 *
 */

router.get(
    '/:myKey',
    storeController.validate('read'),
    storeController.read
)

module.exports = router