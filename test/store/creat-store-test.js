const { expect } = require('chai')
const chance = require('chance')()

const request = require('supertest')
const app = require('../../app')
const Redis = require('../../src/libs/redis')

/* global describe */
/* global it */
/* global before */
/* global after */

const urlEndpoint = '/object'

describe('[Store: Create new store]', () => {

  const redisInstance = new Redis()

  before(done => {
    redisInstance.client.on('ready', () => {
      console.log('I am called in create')
      done()
    })
  })

  it('should fail on invalid api url', done => {
    request(app)
      .post('/abc')
      .set('Accept', 'application/json')
      .send()
      .expect(404, done)
  })

  it('should be able to store the key value object', done => {
    const payload = {}
    const name = chance.name()
    const address = chance.address()
    payload[name] = address

    request(app)
      .post(urlEndpoint)
      .set('Accept', 'application/json')
      .send(payload)
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        expect(res.body).to.be.an('object')
        expect(res.body).to.have.property('key', name)
        expect(res.body).to.have.property('value', address)
        expect(res.body).to.have.property('timestamp')
        done()
      })
  })

  after(done => {
    redisInstance.client.flushallAsync('ASYNC')
      .then(() => {
        done()
      })
  })
})