const { expect } = require('chai')
const chance = require('chance')()

const request = require('supertest')
const app = require('../../app')
const Redis = require('../../src/libs/redis')

const redisInstance = new Redis()

/* global describe */
/* global it */
/* global before */
/* global after */

const urlEndpoint = '/object'

const hashName = process.env.REDIS_HASH_NAME
const setName = process.env.REDIS_SET_NAME
const COUNT = 3
const name = chance.word()

const generateData = num => {

  const data = []

  for (let i = 1; i < num; i += 1) {
    const payload = {}
    const address = chance.address()
    const timestamp = new Date(new Date(2017, 9, i).toUTCString()).getTime()
    payload[name] = address
    payload.timestamp = timestamp

    data.push(payload)
  }

  return data
}

const tempData = generateData(COUNT)
const replicateData = tempData.slice()

const storeDataInRedis = val => {
  if (val) {
    redisInstance.getId('test:id')
      .then(id => {
        const hashWithId = `${hashName}-${id}`

        return redisInstance.setRedis(hashWithId, JSON.stringify(val))
          .then(() => redisInstance.setAdd(setName, val.timestamp, hashWithId))
      })
      .then(() => storeDataInRedis(tempData.shift()))
      .catch(() => {
        storeDataInRedis(tempData.shift())
      })
  }
  else {
    Promise.resolve('Data populated')
    redisInstance.client.emit('completed')
  }

}

describe('[Store: GET values ]', () => {

  before(done => {
    redisInstance.client.on('completed', () => {
      done()
    })
    try {
      storeDataInRedis(tempData.shift())
    } catch (e) {
      done()
    }
  })

  it('should fail on invalid timestamp', done => {
    request(app)
      .get(`/object/${name}?timestamp=123213`)
      .set('Accept', 'application/json')
      .send()
      .expect(422, done)
  })

  it('should be able to get the latest value of given key', done => {

    request(app)
      .get(`${urlEndpoint}/${name}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        const latestData = replicateData[replicateData.length - 1]
        const key = Object.keys(latestData)[0]
        expect(res.body).to.be.an('object')
        expect(res.body).to.have.property('value', latestData[key])
        done()
      })
  })

  it('should get updated value by time', done => {
    const time = new Date(new Date(2017, 9, 2).toUTCString()).getTime()

    request(app)
      .get(`${urlEndpoint}/${name}?timestamp=${time}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) return done(err)
        const latestData = replicateData[0]
        const key = Object.keys(latestData)[0]
        expect(res.body).to.be.an('object')
        expect(res.body).to.have.property('value', latestData[key])
        done()
      })
  })

  after(done => {
    redisInstance.client.flushallAsync('ASYNC')
      .then(() => {
        done()
      })
      .catch(done)
  })
})